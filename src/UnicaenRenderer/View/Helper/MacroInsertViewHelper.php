<?php

namespace UnicaenRenderer\View\Helper;

use Laminas\View\Helper\AbstractHelper;

/**
 *    @desc Génére le bouton de permettant d'inserer des macros dans un inputText
 */
class MacroInsertViewHelper extends AbstractHelper
{
    public function __invoke(): MacroInsertViewHelper
    {
        return $this;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->render();
    }

    /**
     * @return string
     */
    public function render() : string
    {
        return  <<<EOT
    <div id="macro-insert-widget-content">
        <div class="macro-insert-widget float-end">
            <button class="macro-btn btn" type="button"
                    data-bs-title="Insertion d'une macro" data-bs-placement="top">
                <i class="fas fa-stamp" title="Insertion d'une macro..."></i>
            </button>
            <div class="macro-popover">
                <div class="macro-popover-content">
                    <p>
                        <label>Macro :</label><br>
                        <select size="1" tabindex="-1" name="macro-select" class="macro-select">
                            <option value=""></option>
                        </select>
                    </p>
                    <p class="macro-selection-placeholder"></p>
                    <button type="button" class="macro-insert-btn btn btn-primary" onclick="return false;"
                            title="Insérer la macro sélectionnée à l'emplacement actuel du curseur" disabled>
        Insérer
                    </button>
                    <button type="button" class="macro-close-btn btn btn-secondary float-end" onclick="return false;">
            Fermer
                    </button>
                </div>
            </div>
        </div>
    </div>
EOT;

    }

}