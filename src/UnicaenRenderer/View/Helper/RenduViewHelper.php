<?php

namespace UnicaenRenderer\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenRenderer\Entity\Db\Rendu;

class RenduViewHelper extends AbstractHelper
{
    public function __invoke(Rendu $rendu, array $options = []): string|Partial
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('rendu', ['rendu' => $rendu, 'options' => $options]);
    }
}