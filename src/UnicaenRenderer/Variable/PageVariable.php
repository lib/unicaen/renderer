<?php

namespace UnicaenRenderer\Variable;

class PageVariable
{
    const NAME = '__page';

    public function getSautDePage(): string
    {
        return '<pagebreak>'; // balise mPDF
    }
}