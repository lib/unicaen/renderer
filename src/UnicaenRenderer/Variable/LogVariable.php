<?php

namespace UnicaenRenderer\Variable;

use DateTime;

class LogVariable
{
    const NAME = '__log';

    public function getDate(): string
    {
        return (new DateTime())->format('d/m/Y');
    }

    public function getDateEtHeure(): string
    {
        return (new DateTime())->format('d/m/Y H:i:s');
    }
}