<?php

namespace UnicaenRenderer\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenRenderer\Form\Template\TemplateForm;
use UnicaenRenderer\Service\Macro\MacroService;
use UnicaenRenderer\Service\Template\TemplateService;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManager;

class TemplateControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @return TemplateController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): TemplateController
    {
        /**
         * @var TemplateService $templateService
         */
        $templateService = $container->get(TemplateService::class);

        /**
         * @var TemplateForm $contentForm
         */
        $contentForm = $container->get('FormElementManager')->get(TemplateForm::class);

        /** @var MacroService $macroService */
        $macroService = $container->get(MacroService::class);

        /** @var TemplateEngineManager $templateEngineManager */
        $templateEngineManager = $container->get(TemplateEngineManager::class);

        $controller = new TemplateController();
        $controller->setTemplateService($templateService);
        $controller->setMacroService($macroService);
        $controller->setTemplateForm($contentForm);
        $controller->setTemplateEngineManager($templateEngineManager);

        return $controller;
    }
}