<?php

namespace UnicaenRenderer\Controller;

use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenRenderer\Entity\Db\Template;
use UnicaenRenderer\Form\Template\TemplateFormAwareTrait;
use UnicaenRenderer\Service\Macro\MacroServiceAwareTrait;
use UnicaenRenderer\Service\Template\TemplateServiceAwareTrait;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManagerAwareTrait;

class TemplateController extends AbstractActionController
{
    use TemplateServiceAwareTrait;
    use MacroServiceAwareTrait;
    use TemplateFormAwareTrait;
    use TemplateEngineManagerAwareTrait;

    const SANS_NAMESPACE = "Sans namespace";

    public function indexAction(): ViewModel
    {
        $namespace = $this->params()->fromQuery('namespace');
        $namespace = ($namespace !== '')?$namespace:null;
        $type = $this->params()->fromQuery('type');
        $type = ($type !== '')?$type:null;

        $templates = $this->getTemplateService()->getTemplatesByTypeAndNamespaces($type, $namespace);
        $namespaces = $this->getTemplateService()->getNamespaces();
        $types = $this->getTemplateService()->getTypes();

        return new ViewModel([
            'title' => 'Gestion des templates',
            'templates' => $templates,
            'namespaces' => $namespaces,
            'types' => $types,
            'namespace' => $namespace,
            'type' => $type,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $template = $this->getTemplateService()->getRequestedTemplate($this);

        return new ViewModel([
            'title' => "Affichage du template",
            'template' => $template,
        ]);
    }

    public function ajouterAction(): ViewModel|Response
    {
        $engine = $this->params()->fromRoute('engine', Template::TEMPLATE_ENGINE_DEFAULT);

        $template = new Template($engine);

        $form = $this->getTemplateForm();
        $form->setAttribute('action', $this->url()->fromRoute('contenu/template/ajouter', ['engine' => $engine], [], true));
        $form->bind($template);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getTemplateService()->create($template);
                return $this->redirect()->toRoute('contenu/template/modifier', ['template' => $template->getId()], [], true);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-renderer/template/modifier');
        $vm->setVariables([
            'title' => "Création d'un template",
            'form' => $form,
            'template' => $template,
            'macrosJsonValue' => $this->getMacroService()->generateMacrosJsonValueForTemplate($template),
        ]);

        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $template = $this->getTemplateService()->getRequestedTemplate($this);

        $form = $this->getTemplateForm();
        $form->setAttribute('action', $this->url()->fromRoute('contenu/template/modifier', ['template' => $template->getId()], [], true));
        $form->bind($template);
        $form->setOldCode($template->getCode());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getTemplateService()->update($template);
            }
        }

        return new ViewModel([
            'title' => "Modification d'un template",
            'form' => $form,
            'template' => $template,
            'macrosJsonValue' => $this->getMacroService()->generateMacrosJsonValueForTemplate($template),
        ]);
    }

    public function detruireAction(): ViewModel
    {
        $template = $this->getTemplateService()->getRequestedTemplate($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") {
                $this->getTemplateService()->delete($template);
            }
            exit();
        }

        $vm = new ViewModel();
        if ($template !== null) {
            $vm->setTemplate('unicaen-renderer/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du template [" . $template->getCode() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('contenu/template/detruire', ["template" => $template->getId()], [], true),
            ]);
        }
        return $vm;
    }

}