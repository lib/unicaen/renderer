<?php

namespace UnicaenRenderer\Controller;

use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenRenderer\Entity\Db\Macro;
use UnicaenRenderer\Form\Macro\MacroFormAwareTrait;
use UnicaenRenderer\Service\Macro\MacroServiceAwareTrait;
use UnicaenRenderer\Service\Template\TemplateServiceAwareTrait;

class MacroController extends AbstractActionController
{
    use MacroServiceAwareTrait;
    use TemplateServiceAwareTrait;
    use MacroFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $macrosAll = $this->getMacroService()->getMacros();
        $variable = $this->params()->fromQuery('variable');

        $macros = [];
        $variables = [];
        foreach ($macrosAll as $macro) {
            $variables[$macro->getVariable()] = $macro->getVariable();
            if ($variable === null or $variable === '' or $macro->getVariable() === $variable) {
                $macros[] = $macro;
            }
        }

        return new ViewModel([
            'title' => 'Gestion des macros',
            'macros' => $macros,
            'variables' => $variables,
            'variable' => $variable,
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $macro = new Macro();
        $form = $this->getMacroForm();
        $form->setAttribute('action', $this->url()->fromRoute('contenu/macro/ajouter', [], [], true));
        $form->bind($macro);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getMacroService()->create($macro);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-renderer/default/default-form');
        $vm->setVariables([
            'title' => "Ajout d'une nouvelle macro",
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $macro = $this->getMacroService()->getRequestedMacro($this);
        $form = $this->getMacroForm();
        $form->setAttribute('action', $this->url()->fromRoute('contenu/macro/modifier', ['macro' => $macro->getId()], [], true));
        $form->bind($macro);
        $form->setOldCode($macro->getCode());

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getMacroService()->update($macro);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-renderer/default/default-form');
        $vm->setVariables([
            'title' => "Modification d'une macro",
            'form' => $form,
        ]);
        return $vm;
    }

    public function supprimerAction(): ViewModel
    {
        $macro = $this->getMacroService()->getRequestedMacro($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getMacroService()->delete($macro);
            exit();
        }

        $vm = new ViewModel();
        if ($macro !== null) {
            $vm->setTemplate('unicaen-renderer/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la macro " . $macro->getCode(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('contenu/macro/supprimer', ["macro" => $macro->getId()], [], true),
            ]);
        }
        return $vm;
    }

    public function genererJsonAction(): ViewModel
    {
        $engine = $this->params()->fromRoute('engine');
        $json = $this->getMacroService()->generateMacrosJsonForEngineCode($engine);

        return new ViewModel([
            'title' => 'JSON pour tinyMCE',
            'json' => $json,
        ]);
    }

}