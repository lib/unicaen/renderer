<?php

namespace UnicaenRenderer\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenRenderer\Service\Rendu\RenduService;
use UnicaenRenderer\Service\Template\TemplateService;

class RenduControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @return RenduController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): RenduController
    {
        /**
         * @var RenduService $contenuService
         * @var TemplateService $templateService
         */
        $contenuService = $container->get(RenduService::class);
        $templateService = $container->get(TemplateService::class);

        $controller = new RenduController();
        $controller->setRenduService($contenuService);
        $controller->setTemplateService($templateService);
        return $controller;
    }
}