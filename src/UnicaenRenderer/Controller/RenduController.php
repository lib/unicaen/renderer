<?php

namespace UnicaenRenderer\Controller;

use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenRenderer\Service\Rendu\RenduServiceAwareTrait;
use UnicaenRenderer\Service\Template\TemplateServiceAwareTrait;

class RenduController extends AbstractActionController
{
    use RenduServiceAwareTrait;
    use TemplateServiceAwareTrait;

    public function indexAction(): ViewModel
    {
        $templates = $this->getTemplateService()->getTemplates();

        $template = $this->params()->fromQuery('template');
        $date = $this->params()->fromQuery('date');

        $rendus = $this->getRenduService()->getRendusByTemplateAndDate($template, $date);

        return new ViewModel([
            'title' => 'Gestions des rendus',
            'rendus' => $rendus,
            'templates' => $templates,
            'template' => $template,
            'date' => $date,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $rendu = $this->getRenduService()->getRequestedRendu($this);

        return new ViewModel([
            'title' => "Affichage du contenu #" . $rendu->getId(),
            'rendu' => $rendu,
        ]);
    }

    public function supprimerAction(): ViewModel
    {
        $rendu = $this->getRenduService()->getRequestedRendu($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") {
                $this->getRenduService()->delete($rendu);
            }
            exit();
        }

        $vm = new ViewModel();
        if ($rendu !== null) {
            $vm->setTemplate('unicaen-renderer/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du rendu [" . $rendu->getId() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('contenu/contenu/supprimer', ["rendu" => $rendu->getId()], [], true),
            ]);
        }
        return $vm;
    }
}