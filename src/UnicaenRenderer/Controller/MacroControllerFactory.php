<?php

namespace UnicaenRenderer\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenRenderer\Form\Macro\MacroForm;
use UnicaenRenderer\Service\Macro\MacroService;

class MacroControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @return MacroController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): MacroController
    {
        /**
         * @var MacroService $macroService
         */
        $macroService = $container->get(MacroService::class);

        /**
         * @var MacroForm $macroForm
         */
        $macroForm = $container->get('FormElementManager')->get(MacroForm::class);

        $controller = new MacroController();
        $controller->setMacroService($macroService);
        $controller->setMacroForm($macroForm);
        return $controller;
    }
}