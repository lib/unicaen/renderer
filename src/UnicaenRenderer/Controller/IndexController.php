<?php

namespace UnicaenRenderer\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    public function indexAction(): ViewModel
    {
        return new ViewModel(['title' => 'Gestion des contenus']);
    }
}