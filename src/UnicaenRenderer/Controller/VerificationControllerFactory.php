<?php

namespace UnicaenRenderer\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenRenderer\Service\Macro\MacroService;
use UnicaenRenderer\Service\Template\TemplateService;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManager;


class VerificationControllerFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): VerificationController
    {
        /**
         * @var MacroService $macroService
         * @var TemplateService $templateService
         * @var TemplateEngineManager $templateEngineManager
         */
        $macroService = $container->get(MacroService::class);
        $templateService = $container->get(TemplateService::class);
        $templateEngineManager = $container->get(TemplateEngineManager::class);

        $controller = new VerificationController();
        $controller->setMacroService($macroService);
        $controller->setTemplateService($templateService);
        $controller->setTemplateEngineManager($templateEngineManager);

        return $controller;
    }
}