<?php

namespace UnicaenRenderer\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenRenderer\Entity\Db\Macro;
use UnicaenRenderer\Entity\Db\Template;
use UnicaenRenderer\Service\Macro\MacroServiceAwareTrait;
use UnicaenRenderer\Service\Template\TemplateServiceAwareTrait;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManagerAwareTrait;

class VerificationController extends AbstractActionController
{
    use MacroServiceAwareTrait;
    use TemplateServiceAwareTrait;
    use TemplateEngineManagerAwareTrait;

    public function indexAction() : ViewModel
    {
        $macros_ = $this->getMacroService()->getMacros();
        $macros = []; foreach ($macros_ as $macro_) { $macros[$macro_->getCode()] = $macro_;}
        $templates = $this->getTemplateService()->getTemplates();

        //
        // NB : pour l'instant, la vérification d'un template n'existe que pour le moteur 'unicaen'.
        //
        /** @var Template[] $templates */
        $templates = array_filter($templates, fn(Template $t) => $t->getEngine() === Template::TEMPLATE_ENGINE_DEFAULT);

        $macroSujet = $macroCorps = $rMacroSujet = $rMacroCorps = [];
        foreach ($templates as $template) {
            [$macroSujet_, $macroCorps_, $rMacroSujet_, $rMacroCorps_] =
                $this->templateEngineManager->getEngineForTemplate($template)->checkTemplate();
            $macroSujet = array_merge($macroSujet, $macroSujet_);
            $macroCorps = array_merge($macroCorps, $macroCorps_);
            $rMacroSujet = array_merge($rMacroSujet, $rMacroSujet_);
            $rMacroCorps = array_merge($rMacroCorps, $rMacroCorps_);
        }

        $macrosUtiliseesDeclarees = [];
        $macrosUtiliseesNonDeclarees = [];
        foreach ($templates as $template) {
            foreach ($macroSujet[$template->getCode()] as $macro) {
                $code = Macro::extractCode($macro);
                if (!isset($macros[Macro::extractCode($macro)]))  {
                    $macrosUtiliseesNonDeclarees[$code] = $code;
                } else {
                    $macrosUtiliseesDeclarees[$code] = $code;
                }
            }
            foreach ($macroCorps[$template->getCode()] as $macro) {
                $code = Macro::extractCode($macro);
                if (!isset($macros[$code])) {
                    $macrosUtiliseesNonDeclarees[$code] = $code;
                } else {
                    $macrosUtiliseesDeclarees[$code] = $code;
                }
            }
        }

        $macrosDeclareesNonUtilisees = [];
        foreach ($macros as $macro) {
            if (!in_array($macro->getCode(), $macrosUtiliseesDeclarees)) {
                $macrosDeclareesNonUtilisees[] = $macro;
            }
        }

        return new ViewModel([
            'macros' => $macros,
            'templates' => $templates,
            'macroSujet' => $macroSujet,
            'macroCorps' => $macroCorps,
            'rMacroSujet' => $rMacroSujet,
            'rMacroCorps' => $rMacroCorps,

            'macrosUtiliseesNonDeclarees' => $macrosUtiliseesNonDeclarees,
            'macrosDeclareesNonUtilisees' => $macrosDeclareesNonUtilisees,
        ]);
    }
}