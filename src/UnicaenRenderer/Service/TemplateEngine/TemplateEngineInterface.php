<?php

namespace UnicaenRenderer\Service\TemplateEngine;

use UnicaenRenderer\Entity\Db\Macro;
use UnicaenRenderer\Entity\Db\Template;

interface TemplateEngineInterface
{
    /**
     * Spécifie le {@see \UnicaenRenderer\Entity\Db\Template} cible.
     */
    public function setTemplate(Template $template);

    /**
     * Spécifie les variables nécessaires au Template cible.
     */
    public function setVariables(array $variables);

    /**
     * Effectue des vérifications éventuelles concernant le template cible.
     */
    public function checkTemplate();

    /**
     * Génère le rendu du "sujet" du template cible.
     */
    public function renderTemplateSujet(): string;

    /**
     * Génère le rendu du "corps" du template cible.
     */
    public function renderTemplateCorps(): string;

    /**
     * Génère le "code source" qui sera inséré dans le template cible
     * lorsque la macro spécifiée est sélectionnée.
     */
    public function generateMacroSourceCode(Macro $macro): string;
}