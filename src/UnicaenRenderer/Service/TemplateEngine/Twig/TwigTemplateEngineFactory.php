<?php

namespace UnicaenRenderer\Service\TemplateEngine\Twig;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

class TwigTemplateEngineFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): TwigTemplateEngine
    {
        return new TwigTemplateEngine();
    }
}