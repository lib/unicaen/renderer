<?php

namespace UnicaenRenderer\Service\TemplateEngine\Twig;

use BadMethodCallException;
use RuntimeException;
use Twig\Environment;
use Twig\Error\Error;
use Twig\Loader\ArrayLoader;
use UnicaenRenderer\Entity\Db\Macro;
use UnicaenRenderer\Service\TemplateEngine\AbstractTemplateEngine;

class TwigTemplateEngine extends AbstractTemplateEngine
{
    protected ArrayLoader $twigLoader;
    protected Environment $twig;

    public function __construct()
    {
        $this->twigLoader = new ArrayLoader();
        $this->twig = new Environment($this->twigLoader, [
//            'cache' => '/path/to/compilation_cache',
        ]);
    }

    public function checkTemplate(): array
    {
        throw new BadMethodCallException("Non supporté !");
    }

    public function renderTemplateSujet(): string
    {
        $templateName = $this->template->getCode() . '_sujet';
        $this->twigLoader->setTemplate($templateName, $this->template->getSujet());
        try {
            $content = $this->twig->render($templateName, $this->variables);
        } catch (Error $e) {
            throw new RuntimeException("Erreur rencontrée lors de la génération Twig", null, $e);
        }

        return $content;
    }

    public function renderTemplateCorps(): string
    {
        $templateName = $this->template->getCode() . '_corps';
        $this->twigLoader->setTemplate($templateName, $this->template->getCorps());
        try {
            $content = $this->twig->render($templateName, $this->variables);
        } catch (Error $e) {
            throw new RuntimeException("Erreur rencontrée lors de la génération Twig", null, $e);
        }

        $texte = "<style>";
        $texte .= $this->template->getCss();
        $texte .= "</style>";
        $texte .= $content;

        return $texte;
    }

    public function generateMacroSourceCode(Macro $macro): string
    {
        return '{{ ' . $macro->getVariable() . '.' . $macro->getMethode() . ' }}';
    }
}