<?php

namespace UnicaenRenderer\Service\TemplateEngine\Laminas;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

class LaminasTemplateEngineFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): LaminasTemplateEngine
    {
        $service = new LaminasTemplateEngine();

        /* @var $viewHelperManager \Laminas\View\HelperPluginManager */
        $viewHelperManager = $container->get('ViewHelperManager');
        /** @var \Laminas\View\Renderer\PhpRenderer $phpRenderer */
        $phpRenderer = $viewHelperManager->getRenderer();
        $service->setPhpRenderer($phpRenderer);

        return $service;
    }
}