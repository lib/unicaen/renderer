<?php

namespace UnicaenRenderer\Service\TemplateEngine\Laminas;

use BadMethodCallException;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplateMapResolver;
use UnicaenRenderer\Entity\Db\Macro;
use UnicaenRenderer\Service\TemplateEngine\AbstractTemplateEngine;

class LaminasTemplateEngine extends AbstractTemplateEngine
{
    protected PhpRenderer $phpRenderer;

    public function __construct()
    {
        $this->phpRenderer = new PhpRenderer();
    }

    public function setPhpRenderer(PhpRenderer $phpRenderer): void
    {
        $this->phpRenderer = $phpRenderer;
    }

    public function checkTemplate(): array
    {
        throw new BadMethodCallException("Non supporté !");
    }

    public function renderTemplateSujet(): string
    {
        $templateName = $this->template->getCode() . '_sujet';
        $templateContentStream = $this->createContentStreamFromString($this->template->getSujet());

        /** @var \Laminas\View\Resolver\AggregateResolver $resolver */
        $resolver = $this->phpRenderer->resolver();
        $resolver->attach((new TemplateMapResolver())->add($templateName, $templateContentStream));

        return $this->phpRenderer->render($templateName, $this->variables);
    }

    public function renderTemplateCorps(): string
    {
        $templateName = $this->template->getCode() . '_corps';
        $templateContentStream = $this->createContentStreamFromString($this->template->getCorps());

        /** @var \Laminas\View\Resolver\AggregateResolver $resolver */
        $resolver = $this->phpRenderer->resolver();
        $resolver->attach((new TemplateMapResolver())->add($templateName, $templateContentStream));

        $content = $this->phpRenderer->render($templateName, $this->variables);

        $texte = "<style>";
        $texte .= $this->template->getCss();
        $texte .= "</style>";
        $texte .= $content;

        return $texte;
    }

    public function generateMacroSourceCode(Macro $macro): string
    {
        return '<?php echo ' . $macro->getVariable() . '->' . $macro->getMethode() . ' ?>';
    }

    /**
     * Génère le chemin d'un flux "data://" pouvant être utilisé dans le 'include' réalisé
     * par le moteur de template Laminas ({@see \Application\View\Renderer\PhpRenderer::render()}).
     *
     * MAIS ATTENTION, il faut mettre 'allow_url_include' à 'On' dans la config PHP du serveur pour
     * autoriser les 'include' de flux.
     */
    private function createContentStreamFromString(string $templateContent): string
    {
        return "data://text/plain;base64," . base64_encode($templateContent);
    }
}