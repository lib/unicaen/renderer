<?php

namespace UnicaenRenderer\Service\TemplateEngine\Default;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use UnicaenRenderer\Service\Macro\MacroService;

class DefaultTemplateEngineFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): DefaultTemplateEngine
    {
        $service = new DefaultTemplateEngine();

        /** @var \UnicaenRenderer\Service\Macro\MacroService $macroService */
        $macroService = $container->get(MacroService::class);
        $service->setMacroService($macroService);

        return $service;
    }
}