<?php

namespace UnicaenRenderer\Service\TemplateEngine\Default;

use UnicaenRenderer\Entity\Db\Macro;
use UnicaenRenderer\Service\Macro\MacroServiceAwareTrait;
use UnicaenRenderer\Service\TemplateEngine\AbstractTemplateEngine;

class DefaultTemplateEngine extends AbstractTemplateEngine
{
    use MacroServiceAwareTrait;

    public function checkTemplate(): array
    {
        return $this->checkMacros();
    }

    public function renderTemplateSujet(): string
    {
        return $this->replaceMacrosInString($this->template->getSujet());
    }

    public function renderTemplateCorps(): string
    {
        $texte = "<style>" . PHP_EOL;
        $texte .= $this->template->getCss() . PHP_EOL;
        $texte .= "</style>" . PHP_EOL . PHP_EOL;

        $texte .= $this->replaceMacrosInString($this->template->getCorps());

        // On réapplique, mais pourquoi ? Pour les macros qui retournent des macros ? Pour les macros dans le CSS ?
        $this->replaceMacrosInString($texte);

        return $texte;
    }

    public function generateMacroSourceCode(Macro $macro): string
    {
        return 'VAR[' . $macro->getCode() . ']';
    }

    private function replaceMacrosInString(string $string): string
    {
        $matches = [];
        preg_match_all('/VAR\[[^[\]]+#[^[\]]+\]/', $string, $matches);

        $vars = array_unique($matches[0]);
        $replacements = [];
        foreach ($vars as $var) {
            $replacements[] = $this->evaluateVar($var);
        }

        return str_replace($vars, $replacements, $string);
    }

    /**
     * @param string $var Ex : 'VAR[These#Titre]'
     */
    private function evaluateVar(string $var): string
    {
        $code = str_replace('VAR[', '', $var);
        $code = str_replace(']', '', $code);
        $exploded = explode("|", $code);
        $parameter = $exploded[1] ?? null;
        $code = $exploded[0] ?? null;

        $macro = $this->macroService->getMacroByCode($code);
        if ($macro !== null) {
            return $this->evaluateMacro($macro, $parameter);
        }

        return "<span style='color:darkred;'> Macro [" . $code . "] non trouvée en bdd </span>";
    }

    private function evaluateMacro(Macro $macro, $parameter)
    {
        if (isset($this->variables[$macro->getVariable()])) {
            if (method_exists($this->variables[$macro->getVariable()], $macro->getMethode())) {
                if ($parameter !== null) {
                    $texte = $this->variables[$macro->getVariable()]->{$macro->getMethode()}($parameter);
                } else {
                    $texte = $this->variables[$macro->getVariable()]->{$macro->getMethode()}();
                }
                return $texte ?: "";
            }
            return "<span style='color:darkred;'> Méthode [" . $macro->getMethode() . "] non trouvée </span>";
        }
        return "<span style='color:darkred;'> Variable [" . $macro->getVariable() . "] non trouvée </span>";
    }

    private function checkMacros(): array
    {
        $macrosSujet = [];
        $rMacroSujet = [];
        $macrosCorps = [];
        $rMacroCorps = [];

        $sujet = $this->template->getSujet();
        preg_match_all('/VAR\[[^[\]]+#[^[\]]+\]/', $sujet, $matches);
        $macrosSujet[$this->template->getCode()] = $matches[0];
        foreach ($matches[0] as $macro) $rMacroSujet[$macro][$this->template->getCode()] = $this->template->getCode();

        $corps = $this->template->getCorps();
        preg_match_all('/VAR\[[^[\]]+#[^[\]]+\]/', $corps, $matches);
        $macrosCorps[$this->template->getCode()] = $matches[0];
        foreach ($matches[0] as $macro) $rMacroCorps[$macro][$this->template->getCode()] = $this->template->getCode();

        return [$macrosSujet, $macrosCorps, $rMacroSujet, $rMacroCorps];
    }

//    /**
//     * @param Template[] $templates
//     */
//    public function checkMacrosFromTemplates(array $templates): array
//    {
//        $macrosSujet = [];
//        $rMacroSujet = [];
//        $macrosCorps = [];
//        $rMacroCorps = [];
////        $templates = $this->getTemplates();
//        foreach ($templates as $template) {
//            $sujet = $template->getSujet();
//            preg_match_all('/VAR\[[^[\]]+#[^[\]]+\]/', $sujet, $matches);
//            $macrosSujet[$template->getCode()] = $matches[0];
//            foreach ($matches[0] as $macro) $rMacroSujet[$macro][$template->getCode()] = $template->getCode();
//
//            $corps = $template->getCorps();
//            preg_match_all('/VAR\[[^[\]]+#[^[\]]+\]/', $corps, $matches);
//            $macrosCorps[$template->getCode()] = $matches[0];
//            foreach ($matches[0] as $macro) $rMacroCorps[$macro][$template->getCode()] = $template->getCode();
//        }
//
//        return [$macrosSujet, $macrosCorps, $rMacroSujet, $rMacroCorps];
//    }
}