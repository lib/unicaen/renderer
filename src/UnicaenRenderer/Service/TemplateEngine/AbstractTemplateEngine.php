<?php

namespace UnicaenRenderer\Service\TemplateEngine;

use UnicaenRenderer\Entity\Db\TemplateAwareTrait;
use UnicaenRenderer\Variable\LogVariable;
use UnicaenRenderer\Variable\PageVariable;

abstract class AbstractTemplateEngine implements TemplateEngineInterface
{
    use TemplateAwareTrait;

    protected array $variables = [];

    public function setVariables(array $variables): void
    {
        // ajout systématique des variables alimentant les macros utilitaires (ex: saut de page, date courante, etc.)
        $helperVariables = [
            PageVariable::NAME => new PageVariable(),
            LogVariable::NAME => new LogVariable(),
        ];

        $this->variables = array_merge($helperVariables, $variables);
    }


}