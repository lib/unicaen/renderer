<?php

namespace UnicaenRenderer\Service\Template;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenRenderer\Entity\Db\Template;
use UnicaenRenderer\Service\Macro\MacroServiceAwareTrait;

/**
 * @property EntityManager $objectManager
 */
class TemplateService
{
    use MacroServiceAwareTrait;
    use ProvidesObjectManager;


    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Template $template): Template
    {
        $this->getObjectManager()->persist($template);
        $this->getObjectManager()->flush($template);
        return $template;
    }

    public function update(Template $template): Template
    {
        $this->getObjectManager()->flush($template);
        return $template;
    }

    public function delete(Template $template): Template
    {
        $this->getObjectManager()->remove($template);
        $this->getObjectManager()->flush($template);
        return $template;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Template::class)->createQueryBuilder('template');
        return $qb;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return Template[]
     */
    public function getTemplates(string $champ = 'code', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('template.' . $champ, $ordre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return Template[] */
    public function getTemplatesByTypeAndNamespaces(?string $type = null, ?string $namespace = null, string $champ = 'code', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('template.' . $champ, $ordre);
        if ($type !== null) $qb = $qb->andWhere('template.type = :type')->setParameter('type', $type);
        if ($namespace !== null) $qb = $qb->andWhere('template.namespace = :namespace')->setParameter('namespace', $namespace);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return string[] */
    public function getTemplatesAsOptions(string $champ = 'code', string $ordre = 'ASC', ?string $type = null): array
    {
        $qb = $this->createQueryBuilder()->orderBy('template.' . $champ, $ordre);
        if ($type !== null) {
            $qb = $qb->andWhere('template.document_type = :type')
                ->setParameter('type', $type);
        }
        $templates = $qb->getQuery()->getResult();
        $options = [];
        foreach ($templates as $template) {
            $options[$template->getId()] = $template->getCode();
        }
        return $options;
    }

    public function getTemplate(?int $id): ?Template
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('template.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Template partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    public function getRequestedTemplate(AbstractActionController $controller, string $param = 'template'): ?Template
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getTemplate($id);

        return $result;
    }

    public function getTemplateByCode(string $code): ?Template
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('template.code = :code')
            ->setParameter('code', $code);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Template partagent le même code [" . $code . "]", 0, $e);
        }
        return $result;
    }

    /**
     * @return Template[]
     */
    public function getTemplatesByNamespace(?string $namespace): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('template.namespace = :namespace')
            ->setParameter('namespace', $namespace);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getNamespaces(): array
    {
        $templates = $this->getTemplates();
        $namespaces = [];
        foreach ($templates as $template) {
            $namespaces[$template->getNamespace()] = $template->getNamespace();
        }
        sort($namespaces);
        return $namespaces;
    }

    public function getTypes(): array
    {
        $templates = $this->getTemplates();
        $types = [];
        foreach ($templates as $template) {
            $types[$template->getType()] = $template->getType();
        }
        sort($types);
        return $types;
    }
}