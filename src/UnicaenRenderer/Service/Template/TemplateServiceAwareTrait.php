<?php

namespace UnicaenRenderer\Service\Template;

trait TemplateServiceAwareTrait
{

    private TemplateService $templateService;

    public function getTemplateService(): TemplateService
    {
        return $this->templateService;
    }

    public function setTemplateService(TemplateService $templateService): void
    {
        $this->templateService = $templateService;
    }

}