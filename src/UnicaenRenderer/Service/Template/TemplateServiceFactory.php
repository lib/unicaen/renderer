<?php

namespace UnicaenRenderer\Service\Template;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenRenderer\Service\Macro\MacroService;

class TemplateServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): TemplateService
    {
        /**
         * @var EntityManager $entityManager
         * @var MacroService $macroService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $macroService = $container->get(MacroService::class);

        $service = new TemplateService();
        $service->setObjectManager($entityManager);
        $service->setMacroService($macroService);

        return $service;
    }
}