<?php

namespace UnicaenRenderer\Service\Macro;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenRenderer\Entity\Db\Macro;
use UnicaenRenderer\Entity\Db\Template;
use UnicaenRenderer\Service\TemplateEngine\TemplateEngineInterface;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManagerAwareTrait;

/**
 * @property EntityManager $objectManager
 */
class MacroService
{
    use ProvidesObjectManager;
    use TemplateEngineManagerAwareTrait;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Macro $macro): Macro
    {
        $this->getObjectManager()->persist($macro);
        $this->getObjectManager()->flush($macro);
        return $macro;
    }

    public function update(Macro $macro): Macro
    {
        $this->getObjectManager()->flush($macro);
        return $macro;
    }

    public function historise(Macro $macro): Macro
    {
        $this->getObjectManager()->flush($macro);
        return $macro;
    }

    public function restore(Macro $macro): Macro
    {
        $this->getObjectManager()->flush($macro);
        return $macro;
    }

    public function delete(Macro $macro): Macro
    {
        $this->getObjectManager()->remove($macro);
        $this->getObjectManager()->flush($macro);
        return $macro;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Macro::class)->createQueryBuilder('macro');
        return $qb;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return Macro[]
     */
    public function getMacros(string $champ = 'code', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('macro.' . $champ, $ordre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getMacro(?int $id): ?Macro
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('macro.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Macro partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    public function getMacroByCode(?string $code): ?Macro
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('macro.code = :code')
            ->setParameter('code', $code);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Macro partagent le même code [" . $code . "]", 0, $e);
        }
        return $result;
    }

    public function getRequestedMacro(AbstractActionController $controller, string $param = 'macro'): ?Macro
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getMacro($id);
        return $result;
    }

    /** FACADE ********************************************************************************************************/

    public function generateMacrosJsonValueForTemplate(Template $template): string
    {
        $templateEngine = $this->templateEngineManager->getEngineForTemplate($template);
        return $this->generateMacrosJsonForEngine($templateEngine);
    }

    public function generateMacrosJsonForEngineCode(string $engineCode): string
    {
        $templateEngine = $this->templateEngineManager->get($engineCode);
        return $this->generateMacrosJsonForEngine($templateEngine);
    }

    public function generateMacrosJsonForEngine(TemplateEngineInterface $templateEngine): string
    {
        $macros = $this->getMacros();

        $array = [];
        foreach ($macros as $macro) {
            //$description = $macro->getDescription()?strip_tags(str_replace("'", "\'", $macro->getDescription())):"";
            $array[] = [
                'id' => $macro->getCode(),
                'text' => $macro->getCode(),
                'description' => $macro->getDescription() ?: 'Aucune description',
                'value' => $templateEngine->generateMacroSourceCode($macro),
            ];
        }

        return json_encode($array, JSON_HEX_APOS | JSON_HEX_QUOT);
    }
}