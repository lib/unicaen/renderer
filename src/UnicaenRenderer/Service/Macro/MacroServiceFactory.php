<?php

namespace UnicaenRenderer\Service\Macro;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManager;

class MacroServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): MacroService
    {
        /**
         * @var EntityManager $entityManager
         * @var TemplateEngineManager $templateEngineManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $templateEngineManager = $container->get(TemplateEngineManager::class);

        $service = new MacroService();
        $service->setObjectManager($entityManager);
        $service->setTemplateEngineManager($templateEngineManager);

        return $service;
    }
}