<?php

namespace UnicaenRenderer\Service\Macro;

trait MacroServiceAwareTrait
{

    private MacroService $macroService;

    public function getMacroService(): MacroService
    {
        return $this->macroService;
    }

    public function setMacroService(MacroService $macroService): void
    {
        $this->macroService = $macroService;
    }


}