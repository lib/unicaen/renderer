<?php

namespace UnicaenRenderer\Service\Rendu;

use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Exception;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Renderer\PhpRenderer;
use Mpdf\MpdfException;
use RuntimeException;
use UnicaenPdf\Exporter\PdfExporter;
use UnicaenRenderer\Entity\Db\Rendu;
use UnicaenRenderer\Entity\Db\Template;
use UnicaenRenderer\Service\Template\TemplateServiceAwareTrait;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManagerAwareTrait;

/**
 * @property EntityManager $objectManager
 */
class RenduService
{
    use TemplateServiceAwareTrait;
    use ProvidesObjectManager;
    use TemplateEngineManagerAwareTrait;

    public PhpRenderer $renderer;

    /** Gestion des entités *******************************************************************************************/

    public function create(Rendu $contenu): Rendu
    {
        $this->getObjectManager()->persist($contenu);
        $this->getObjectManager()->flush($contenu);
        return $contenu;
    }

    public function update(Rendu $contenu): Rendu
    {
        $this->getObjectManager()->flush($contenu);
        return $contenu;
    }

    public function delete(Rendu $contenu): Rendu
    {
        $this->getObjectManager()->remove($contenu);
        $this->getObjectManager()->flush($contenu);
        return $contenu;
    }

    /** Requetages ****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Rendu::class)->createQueryBuilder('contenu')
            ->leftJoin('contenu.template', 'template')->addSelect('template');
        return $qb;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return Rendu[]
     */
    public function getRendus(string $champ = 'date', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('contenu.' . $champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return Rendu[] */
    public function getRendusByTemplateAndDate(?string $template = null, ?string $date = null, string $champ = 'date', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()->orderBy('contenu.' . $champ, $ordre);

        if ($template !== null && trim($template) !== '') $qb = $qb->andWhere('template.code = :code')->setParameter('code', $template);
        if ($date !== null && trim($date) !== '') {
            try {
                $date_ = (new DateTime())->sub(new DateInterval('P' . $date));
            } catch (Exception $e) {
                throw new RuntimeException("Problème de calcul de la date buttoir avec [" . $date . "]", 0, $e);
            }
            $qb = $qb->andWhere('contenu.date >= :date')->setParameter('date', $date_);
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }


    public function getRendu(?int $id): ?Rendu
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('contenu.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Sursis partagent le même id [" . $id . "]", 0, $e);
        }

        return $result;
    }

    public function getRequestedRendu(AbstractActionController $controller, string $param = 'rendu'): ?Rendu
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getRendu($id);
        return $result;
    }

    /** facade ********************************************************************************************************/

    public function generateRenduByTemplate(Template $template, array $variables, bool $save = true): Rendu
    {
        $contenu = new Rendu();
        $contenu->setTemplate($template);
        $contenu->setDate(new DateTime());

        $templateEngine = $this->templateEngineManager->getEngineForTemplate($template);
        $templateEngine->setVariables($variables);
        $sujet = $templateEngine->renderTemplateSujet();
        $corps = $templateEngine->renderTemplateCorps();
        $contenu->setSujet($sujet);
        $contenu->setCorps($corps);

        if ($save) {
            $this->create($contenu);
        }

        return $contenu;
    }

    public function generateRenduByTemplateCode(string $code, array $variables = [], bool $save = true): Rendu
    {
        $template = $this->getTemplateService()->getTemplateByCode($code);
        if ($template === null) throw new RuntimeException('Aucun template de trouvé avec le code [' . $code . ']');

        return $this->generateRenduByTemplate($template, $variables, $save);
    }




    /**
     * TODO >>> Dans unicaenPDF
     * @deprecated À supprimer puisqu'il existe unicaen/pdf
     * @see PdfExporter::generatePdf() la seule différence est en effet les templates des entetes et pieds de pages (qui n'ont pas de valeur par défaut (par grave en soit))
     */
    public function generate(string $sujet, string $corps, string $header = null, string $footer = null,
                             string $destination = PdfExporter::DESTINATION_BROWSER, string $filename = 'export.pdf'): ?string
    {
        if ($header === null) $footer = '/unicaen-renderer/pdf/header';
        if ($footer === null) $footer = '/unicaen-renderer/pdf/footer';

        $exporter = new PdfExporter($this->renderer);
        return $exporter->generatePdf($filename, $sujet, $corps, $header, $footer);
    }

    /** @return Rendu[] */
    public function getRendusOlderThan(DateTime $deadline): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('contenu.date < :deadline')->setParameter('deadline', $deadline);
        $result = $qb->getQuery()->getResult();
        return $result;
    }
}

