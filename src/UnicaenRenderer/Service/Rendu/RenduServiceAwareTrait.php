<?php

namespace UnicaenRenderer\Service\Rendu;

trait RenduServiceAwareTrait
{

    private RenduService $contenuService;

    public function getRenduService(): RenduService
    {
        return $this->contenuService;
    }

    public function setRenduService(RenduService $contenuService): void
    {
        $this->contenuService = $contenuService;
    }

}