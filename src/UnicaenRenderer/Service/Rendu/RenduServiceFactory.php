<?php

namespace UnicaenRenderer\Service\Rendu;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenRenderer\Service\Template\TemplateService;
use Laminas\View\Renderer\PhpRenderer;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManager;

class RenduServiceFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : RenduService
    {
        /**
         * @var EntityManager $entityManager
         * @var TemplateService $templateService
         * @var \UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManager $templateEngineManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $templateService = $container->get(TemplateService::class);
        $templateEngineManager = $container->get(TemplateEngineManager::class);

        $service = new RenduService();
        $service->setObjectManager($entityManager);
        $service->setTemplateService($templateService);
        $service->setTemplateEngineManager($templateEngineManager);

        /* @var PhpRenderer $renderer  */
        $renderer = $container->get('ViewRenderer');

        $service->renderer = $renderer;
        return $service;
    }
}