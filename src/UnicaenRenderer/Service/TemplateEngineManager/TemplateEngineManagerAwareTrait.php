<?php

namespace UnicaenRenderer\Service\TemplateEngineManager;

trait TemplateEngineManagerAwareTrait
{
    protected TemplateEngineManager $templateEngineManager;

    public function setTemplateEngineManager(TemplateEngineManager $templateEngineManager): void
    {
        $this->templateEngineManager = $templateEngineManager;
    }
}