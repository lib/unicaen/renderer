<?php

namespace UnicaenRenderer\Service\TemplateEngineManager;

use Laminas\ServiceManager\AbstractPluginManager;
use UnicaenRenderer\Entity\Db\Template;
use UnicaenRenderer\Service\TemplateEngine\Default\DefaultTemplateEngine;
use UnicaenRenderer\Service\TemplateEngine\Default\DefaultTemplateEngineFactory;
use UnicaenRenderer\Service\TemplateEngine\Laminas\LaminasTemplateEngine;
use UnicaenRenderer\Service\TemplateEngine\Laminas\LaminasTemplateEngineFactory;
use UnicaenRenderer\Service\TemplateEngine\TemplateEngineInterface;
use UnicaenRenderer\Service\TemplateEngine\Twig\TwigTemplateEngine;
use UnicaenRenderer\Service\TemplateEngine\Twig\TwigTemplateEngineFactory;

class TemplateEngineManager extends AbstractPluginManager
{
    protected $instanceOf = TemplateEngineInterface::class;

    protected $factories = [
        DefaultTemplateEngine::class => DefaultTemplateEngineFactory::class,
        LaminasTemplateEngine::class => LaminasTemplateEngineFactory::class,
        TwigTemplateEngine::class => TwigTemplateEngineFactory::class,
    ];

    protected $aliases = [
        Template::TEMPLATE_ENGINE_DEFAULT => DefaultTemplateEngine::class,
        Template::TEMPLATE_ENGINE_LAMINAS => LaminasTemplateEngine::class,
        Template::TEMPLATE_ENGINE_TWIG => TwigTemplateEngine::class,
    ];

    /**
     * Retourne l'instance du moteur de template compétent pour le template spécifié.
     * NB : Le template en question est injecté d'office dans l'instance retournée.
     */
    public function getEngineForTemplate(Template $template): TemplateEngineInterface
    {
        /** @var TemplateEngineInterface $templateEngine */
        $templateEngine = $this->get($template->getEngine());
        $templateEngine->setTemplate($template);

        return $templateEngine;
    }
}