<?php

namespace UnicaenRenderer\Service\TemplateEngineManager;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

class TemplateEngineManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): TemplateEngineManager
    {
        return new TemplateEngineManager($container);
    }
}