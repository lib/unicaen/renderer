<?php

namespace UnicaenRenderer\Entity\Db;

use InvalidArgumentException;
use Laminas\Permissions\Acl\Resource\ResourceInterface;

class Template implements ResourceInterface
{
    const RESOURCE_ID = 'Template';

    public function generateTag(): string
    {
        return (implode('_', [$this->getResourceId(), $this->getCode()]));
    }

    public function getResourceId(): string
    {
        return self::RESOURCE_ID;
    }

    const TYPE_TXT = 'texte';
    const TYPE_PDF = 'pdf';
    const TYPE_MAIL = 'mail';
    const TYPES = [
        self::TYPE_MAIL => "Template de mail",
        self::TYPE_PDF => "Template de pdf",
        self::TYPE_TXT => "Template de texte",
    ];

    // moteurs de rendu disponibles
    const TEMPLATE_ENGINE_DEFAULT = 'default';
    const TEMPLATE_ENGINE_LAMINAS = 'laminas';
    const TEMPLATE_ENGINE_TWIG = 'twig';
    const TEMPLATE_ENGINES = [
        self::TEMPLATE_ENGINE_DEFAULT => "Moteur par défaut",
        self::TEMPLATE_ENGINE_LAMINAS => "Moteur Laminas (PhpRenderer)",
        self::TEMPLATE_ENGINE_TWIG => "Moteur Twig",
    ];

    private ?int $id = null;
    private ?string $code = null;
    private string $engine;
    private ?string $description = null;
    private ?string $namespace = null;
    private ?string $type = null;
    private ?string $sujet = null;
    private ?string $corps = null;
    private ?string $css = null;

    public function __construct(string $engine = self::TEMPLATE_ENGINE_DEFAULT)
    {
        $this->engine = $engine;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getEngine(): string
    {
        return $this->engine;
    }

    public function setEngine(string $engine): self
    {
        if (!in_array($engine, array_keys(self::TEMPLATE_ENGINES))) {
            throw new InvalidArgumentException("Moteur de template inconnu : " . $engine);
        }

        $this->engine = $engine;

        return $this;
    }

    /**
     * Seuls les templates utilisant le moteur par défaut peuvent être édités avec TinyMCE.
     * Les autres moteurs (PhpRenderer et Twig) nécessitent l'édition manuelle en HTML.
     */
    public function requiresTextEditing(): bool
    {
        return $this->getEngine() !== Template::TEMPLATE_ENGINE_DEFAULT;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getNamespace(): ?string
    {
        return $this->namespace;
    }

    public function setNamespace(?string $namespace): void
    {
        $this->namespace = $namespace;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getSujet(): ?string
    {
        return $this->sujet;
    }

    public function setSujet(?string $sujet): void
    {
        $this->sujet = $sujet;
    }

    public function getCorps(): ?string
    {
        return $this->corps;
    }

    public function setCorps(?string $corps): void
    {
        $this->corps = $corps;
    }

    public function getCss(): ?string
    {
        return $this->css;
    }

    public function setCss(?string $css): void
    {
        $this->css = $css;
    }

}