<?php

namespace UnicaenRenderer\Entity\Db;

use Laminas\Permissions\Acl\Resource\ResourceInterface;

class Macro implements ResourceInterface
{
    const RESOURCE_ID = 'Macro';

    public function getResourceId(): string
    {
        return self::RESOURCE_ID;
    }

    private ?int $id = null;
    private ?string $code = null;
    private ?string $variable = null;
    private ?string $description = null;
    private ?string $methode = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getVariable(): ?string
    {
        return $this->variable;
    }

    public function setVariable(?string $variable): void
    {
        $this->variable = $variable;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getMethode(): ?string
    {
        return $this->methode;
    }

    public function setMethode(?string $methode): void
    {
        $this->methode = $methode;
    }


    public static function extractCode(string $macro) : string
    {
        $code = explode("|",substr($macro,4,-1))[0];
        return $code;
    }
}