<?php

namespace UnicaenRenderer\Entity\Db;

use DateTime;
use Laminas\Permissions\Acl\Resource\ResourceInterface;

class Rendu implements ResourceInterface
{
    const RESOURCE_ID = 'Rendu';

    public function getResourceId(): string
    {
        return self::RESOURCE_ID;
    }

    private ?int $id = null;
    private ?Template $template = null;
    private ?DateTime $date = null;
    private ?string $sujet = null;
    private ?string $corps = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTemplate(): ?Template
    {
        return $this->template;
    }

    public function setTemplate(?Template $template): void
    {
        $this->template = $template;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(?DateTime $date): void
    {
        $this->date = $date;
    }

    public function getSujet(): string
    {
        return $this->sujet;
    }

    public function setSujet(?string $sujet): void
    {
        $this->sujet = $sujet;
    }

    public function getCorps(): ?string
    {
        return $this->corps;
    }

    public function setCorps(?string $corps): void
    {
        $this->corps = $corps;
    }
}