<?php

namespace UnicaenRenderer\Entity\Db;

trait TemplateAwareTrait
{
    protected Template $template;

    public function setTemplate(Template $template): void
    {
        $this->template = $template;
    }
}