<?php

namespace UnicaenRenderer\Form\Template;

use Interop\Container\ContainerInterface;

class TemplateHydratorFactory
{

    public function __invoke(ContainerInterface $container): TemplateHydrator
    {
        $hydrator = new TemplateHydrator();
        return $hydrator;
    }

}