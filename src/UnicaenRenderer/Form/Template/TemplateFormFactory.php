<?php

namespace UnicaenRenderer\Form\Template;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenRenderer\Entity\Db\Template;
use UnicaenRenderer\Service\Macro\MacroService;

class TemplateFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): TemplateForm
    {
        /**
         * @var EntityManager $entityManager
         * @var MacroService $macroService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $macroService = $container->get(MacroService::class);

        /** @var TemplateHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(TemplateHydrator::class);

        $form = new TemplateForm();
        $form->setObjectManager($entityManager);
        $form->setMacroService($macroService);
        $form->setHydrator($hydrator);

        $form->setObject(new Template());

        return $form;
    }
}