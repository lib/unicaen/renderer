<?php

namespace UnicaenRenderer\Form\Template;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\Form\FormInterface;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use UnicaenRenderer\Entity\Db\Template;
use UnicaenRenderer\Service\Macro\MacroServiceAwareTrait;

/**
 * @property EntityManager $objectManager
 * @method Template getObject()
 */
class TemplateForm extends Form
{
    use MacroServiceAwareTrait;
    use ProvidesObjectManager;

    public function init(): void
    {
        // code
        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code du contenu <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);
        $this->add([
            'name' => 'old-code',
            'type' => Hidden::class,
            'attributes' => [
                'value' => "",
            ],
        ]);
        // moteur
        $this->add([
            'type' => Select::class,
            'name' => 'engine',
            'options' => [
                'label' => "Moteur de template  <span class='icon icon-obligatoire' title='Champ obligatoire'></span> : ",
                'label_options' => [ 'disable_html_escape' => true, ],
                'value_options' => Template::TEMPLATE_ENGINES,
            ],
            'attributes' => [
                'id' => 'engine',
            ],
        ]);
        //type
        $this->add([
            'name' => 'type',
            'type' => Select::class,
            'options' => [
                'label' => "Type  <span class='icon icon-obligatoire' title='Champ obligatoire'></span> : ",
                'label_options' => [ 'disable_html_escape' => true, ],
                'empty_option' => "Sélectionner un type de contenu",
                'value_options' => [
                    Template::TYPE_PDF => "Fichier PDF",
                    Template::TYPE_TXT => "Template textuel",
                    Template::TYPE_MAIL => "Courrier électronique",
                ],
            ],
        ]);
        //description
        $this->add([
            'name' => 'description',
            'type' => 'textarea',
            'options' => [
                'label' => 'Description : ',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'id' => 'description',
                'class' => 'description form-control',
            ]
        ]);
        // namespace
        $this->add([
            'type' => Text::class,
            'name' => 'namespace',
            'options' => [
                'label' => "Namespace associé :",
            ],
            'attributes' => [
                'id' => 'namespace',
            ],
        ]);
        //complement
        $this->add([
            'name' => 'sujet',
            'type' => 'textarea',
            'options' => [
                'label' => "Sujet (sujet du courrier électronique, nom du fichier, étiquette, ... )  <span class='icon icon-obligatoire' title='Champ obligatoire'></span> : ",
                'label_options' => [ 'disable_html_escape' => true, ],
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'id' => 'sujet',
                'class' => 'form-control sujet macro-compatible',
            ]
        ]);
        //template
        $this->add([
            'name' => 'corps',
            'type' => 'textarea',
            'options' => [
                'label' => "Corps  <span class='icon icon-obligatoire' title='Champ obligatoire'></span>  : ",
                'label_options' => [ 'disable_html_escape' => true, ],
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'id' => 'corps',
                'class' => 'form-control corps macro-compatible',
                'rows' => 10,
            ]
        ]);
        //css
        $this->add([
            'name' => 'css',
            'type' => 'textarea',
            'options' => [
                'label' => 'Feuille de style (CSS) : ',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'id' => 'css',
                'class' => 'form-control css',
                'rows' => 6,
            ]
        ]);
        // button
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'code' => [
                'required' => true,
                'validators' => [[
                    'name' => Callback::class,
                    'options' => [
                        'messages' => [
                            Callback::INVALID_VALUE => "Ce code existe déjà",
                        ],
                        'callback' => function ($value, $context = []) {
                            if ($value == $context['old-code']) return true;
                            return ($this->objectManager->getRepository(Template::class)->findOneBy(['code' => $value], []) == null);
                        },
                        //'break_chain_on_failure' => true,
                    ],
                ]],
            ],
            'old-code' => ['required' => false,],
            'type' => ['required' => true,],
            'engine' => ['required' => true,],
            'description' => ['required' => false,],
            'namespace' => ['required' => false,],
            'sujet' => ['required' => true,],
            'corps' => ['required' => true,],
            'css' => ['required' => false,],

        ]));
    }

    public function bind(object $object, int $flags = FormInterface::VALUES_NORMALIZED): static
    {
        parent::bind($object, $flags);

        // restreint la liste déroulante au moteur spécifié dans le Template
        /** @var \Laminas\Form\Element\Select $engineSelect */
        $engineSelect = $this->get('engine');
        $engineSelect
            ->setEmptyOption(null)
            ->setValueOptions(array_filter(
                $engineSelect->getValueOptions(),
                fn($k) => $k === $this->getObject()->getEngine(),
                ARRAY_FILTER_USE_KEY
            ));

        return $this;
    }

    public function setOldCode($value): void
    {
        $this->get('old-code')->setValue($value);
    }
}