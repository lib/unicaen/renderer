<?php

namespace UnicaenRenderer\Form\Template;

use Laminas\Hydrator\HydratorInterface;
use UnicaenRenderer\Entity\Db\Template;

class TemplateHydrator implements HydratorInterface
{

    public function extract(object $object): array
    {
        /** @var Template $object */
        $data = [
            'code' => ($object) ? $object->getCode() : null,
            'type' => ($object) ? $object->getType() : null,
            'engine' => ($object) ? $object->getEngine() : null,
            'description' => ($object) ? $object->getDescription() : null,
            'namespace' => ($object) ? $object->getNamespace() : null,
            'sujet' => ($object) ? $object->getSujet() : null,
            'corps' => ($object) ? $object->getCorps() : null,
            'css' => ($object) ? $object->getCss() : null,
        ];

        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $code = (isset($data['code']) and trim($data['code']) !== "") ? trim($data['code']) : null;
        $type = (isset($data['type']) and trim($data['type']) !== "") ? trim($data['type']) : null;
        $engine = (isset($data['engine']) and trim($data['engine']) !== "") ? trim($data['engine']) : null;
        $description = (isset($data['description']) and trim($data['description']) !== "") ? trim($data['description']) : null;
        $namespace = (isset($data['namespace']) and trim($data['namespace']) !== "") ? trim($data['namespace']) : null;
        $sujet = (isset($data['sujet']) and trim($data['sujet']) !== "") ? strip_tags(trim($data['sujet'])) : null;
        $corps = (isset($data['corps']) and trim($data['corps']) !== "") ? trim($data['corps']) : null;
        $css = (isset($data['css']) and trim($data['css']) !== "") ? strip_tags(trim($data['css'])) : null;

        /** @var Template $object */
        $object->setCode($code);
        $object->setType($type);
        $object->setEngine($engine);
        $object->setDescription($description);
        $object->setNamespace($namespace);
        $object->setSujet($sujet);
        $object->setCorps($corps);
        $object->setCss($css);

        return $object;
    }

}