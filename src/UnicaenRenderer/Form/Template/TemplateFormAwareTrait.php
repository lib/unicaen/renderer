<?php

namespace UnicaenRenderer\Form\Template;

trait TemplateFormAwareTrait
{

    private TemplateForm $templateForm;

    public function getTemplateForm(): TemplateForm
    {
        return $this->templateForm;
    }

    public function setTemplateForm(TemplateForm $templateForm): void
    {
        $this->templateForm = $templateForm;
    }

}