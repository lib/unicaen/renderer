<?php

namespace UnicaenRenderer\Form\Macro;

trait MacroFormAwareTrait
{
    private MacroForm $macroForm;

    public function getMacroForm(): MacroForm
    {
        return $this->macroForm;
    }

    public function setMacroForm(MacroForm $macroForm): void
    {
        $this->macroForm = $macroForm;
    }
}