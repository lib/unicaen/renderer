<?php

namespace UnicaenRenderer\Form\Macro;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use UnicaenRenderer\Entity\Db\Macro;

/**
 * @property EntityManager $objectManager
 */
class MacroForm extends Form
{
    use ProvidesObjectManager;

    public function init(): void
    {
        // code
        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code de la macro * :",
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);
        $this->add([
            'name' => 'old-code',
            'type' => Hidden::class,
            'attributes' => [
                'value' => "",
            ],
        ]);
        // variable
        $this->add([
            'type' => Text::class,
            'name' => 'variable',
            'options' => [
                'label' => "Variable associée <span class='icon icon-obligatoire' title='Champ obligatoire'></span>  : ",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'variable',
            ],
        ]);
        // méthode
        $this->add([
            'type' => Text::class,
            'name' => 'methode',
            'options' => [
                'label' => "Méthode associée <span class='icon icon-obligatoire' title='Champ obligatoire'></span>  : ",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'methode',
            ],
        ]);
        //description
        $this->add([
            'type' => Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => "Description :",
            ],
            'attributes' => [
                'id' => 'description',
                'class' => "type2",
            ],
        ]);
        // button
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'code' => [
                'required' => true,
                'validators' => [[
                    'name' => Callback::class,
                    'options' => [
                        'messages' => [
                            Callback::INVALID_VALUE => "Ce code existe déjà",
                        ],
                        'callback' => function ($value, $context = []) {
                            if ($value == $context['old-code']) return true;
                            return ($this->objectManager->getRepository(Macro::class)->findOneBy(['code' => $value], []) == null);
                        },
                        //'break_chain_on_failure' => true,
                    ],
                ]],
            ],
            'old-code' => ['required' => false,],
            'variable' => ['required' => true,],
            'methode' => ['required' => true,],
            'description' => ['required' => false,],
        ]));
    }

    public function setOldCode($value): void
    {
        $this->get('old-code')->setValue($value);
    }

}