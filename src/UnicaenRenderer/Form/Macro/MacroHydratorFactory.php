<?php

namespace UnicaenRenderer\Form\Macro;

use Interop\Container\ContainerInterface;

class MacroHydratorFactory
{
    public function __invoke(ContainerInterface $container): MacroHydrator
    {
        $hydrator = new MacroHydrator();
        return $hydrator;
    }
}