<?php

namespace UnicaenRenderer\Form\Macro;

use Laminas\Hydrator\HydratorInterface;
use UnicaenRenderer\Entity\Db\Macro;

class MacroHydrator implements HydratorInterface
{
    public function extract(object $object): array
    {
        /** @var Macro $object */
        $data = [
            'code' => $object->getCode(),
            'variable' => $object->getVariable(),
            'methode' => $object->getMethode(),
            'description' => $object->getDescription(),
        ];
        return $data;
    }

    public function hydrate(array $data, $object): object
    {
        $code = (isset($data['code']) and trim($data['code']) !== null) ? trim($data['code']) : null;
        $variable = (isset($data['variable']) and trim($data['variable']) !== null) ? trim($data['variable']) : null;
        $methode = (isset($data['methode']) and trim($data['methode']) !== null) ? trim($data['methode']) : null;
        $description = (isset($data['description']) and trim($data['description']) !== null) ? trim($data['description']) : null;

        /** @var Macro $object */
        $object->setCode($code);
        $object->setVariable($variable);
        $object->setMethode($methode);
        $object->setDescription($description);

        return $object;
    }

}