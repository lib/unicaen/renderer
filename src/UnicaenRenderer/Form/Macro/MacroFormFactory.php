<?php

namespace UnicaenRenderer\Form\Macro;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class MacroFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): MacroForm
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        /**
         * @var MacroHydrator $hydrator
         */
        $hydrator = $container->get('HydratorManager')->get(MacroHydrator::class);

        $form = new MacroForm();
        $form->setObjectManager($entityManager);
        $form->setHydrator($hydrator);
        return $form;
    }
}