<?php

namespace UnicaenRenderer\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class DocumentmacroPrivileges extends Privileges
{
    const DOCUMENTMACRO_INDEX = 'documentmacro-documentmacro_index';
    const DOCUMENTMACRO_AJOUTER = 'documentmacro-documentmacro_ajouter';
    const DOCUMENTMACRO_MODIFIER = 'documentmacro-documentmacro_modifier';
    const DOCUMENTMACRO_SUPPRIMER = 'documentmacro-documentmacro_supprimer';
}