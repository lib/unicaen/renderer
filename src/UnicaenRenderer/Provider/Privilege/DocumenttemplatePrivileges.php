<?php

namespace UnicaenRenderer\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class DocumenttemplatePrivileges extends Privileges
{
    const DOCUMENTTEMPLATE_INDEX        = 'documenttemplate-documenttemplate_index';
    const DOCUMENTTEMPLATE_AJOUTER      = 'documenttemplate-documenttemplate_ajouter';
    const DOCUMENTTEMPLATE_AFFICHER     = 'documenttemplate-documenttemplate_afficher';
    const DOCUMENTTEMPLATE_MODIFIER     = 'documenttemplate-documenttemplate_modifier';
    const DOCUMENTTEMPLATE_SUPPRIMER    = 'documenttemplate-documenttemplate_supprimer';
}