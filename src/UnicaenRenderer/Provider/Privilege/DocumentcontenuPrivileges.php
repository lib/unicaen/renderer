<?php

namespace UnicaenRenderer\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class DocumentcontenuPrivileges extends Privileges
{
    const DOCUMENTCONTENU_INDEX         = 'documentcontenu-documentcontenu_index';
    const DOCUMENTCONTENU_AFFICHER      = 'documentcontenu-documentcontenu_afficher';
    const DOCUMENTCONTENU_SUPPRIMER     = 'documentcontenu-documentcontenu_supprimer';
}