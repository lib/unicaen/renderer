Module Unicaen Evenement
=======================

Description
-----------

La bibliothèque `unicaen/renderer` est en charge de la génération de contenus "génériques" basés sur des `templates` et `macros`. 

La version 7.0.0 introduit la notion de _moteur de template_ (engine).
Plusieurs moteurs de template sont proposés, c'est à dire plusieurs formats d'écriture : 
- le moteur par défaut : 
  - c'est le moteur historique et le plus facile pour un utlisateur "métier",
  - l'éditeur WYSIWYG TinyMCE est activé pour que l'utilisateur rédige et mette en forme
    presque tout seul le corps et le sujet du template,
  - les macros insérées sont de la forme `VAR[Aaaaa#bbbbb]` ;
- le moteur Laminas (PhpRenderer) :
  - c'est le moteur standard du wramework Lamimas
  - la rédaction du template se fait manuellement au format HTML,
  - les macros insérées sont de la forme `<?php echo $aaaa->bbbbb() ?>` ;
- le moteur Twig (https://twig.symfony.com) :
  - la rédaction du template se fait manuellement au format HTML,
  - les macros insérées sont de la forme `{{ ... }}` ;


Dépendances javascript
----------------------

- Le composant [Select2](https://select2.org) est requis (testé avec la v4.0.13).
- Le moteur de template par défaut nécessite l'éditeur [TinyMCE](https://www.tiny.cloud/) (testé avec la v6.3.0 et la v7.6.0). 

Si besoin, vous trouverez dans le répertoire [res/](res) les versions utilisées lors des tests.


Utilisation de la bibliothèque
-----------

Le service `RenduService` fournit une méthode `generateRenduByTemplateCode` en charge de générer un contenu avec le code d'un temmplate et un tableau de variables qui seront exploitées par les macros.
Le troisième paramètre permet de controller l'enregistrement du rendu en base de données.

```php
$vars = ['variable1' => $variable1, ...];
$rendu = $this->getRenduService()->generateRenduByTemplateCode(Templates::MON_TEMPLATE, $vars, false);

echo $rendu->getSujet();
echo $rendu->getCorps();
```

Aides de vue founies
--------------------

Un `ViewHelper` pour l'affichage d'un rendu

```php
echo $this->rendu($rendu); 
```

Un `ViewHelper` pour l'ajout des macros dans un input text ou textarea de class .macro-compatible

```php
echo $this->macroInsert(); 
```
_Remarque :_
A faire en fin de fichier une seul fois, le JS s'occupe de l'associer aux différents input

Administration
--------------

La biliothèque fournit des interfaces d'administration pour le paramètre des macros et templates et le listing des rendus.
Le fichier `config/unicae-renderer.global.dist.php` contient la déclaration des différents menus d'administration.

Tables et privilèges associés
============================

Les tables et les privilèges sont fournis dans les fichiers suivants : `SQL/001_tables.sql` et `SQL/002_privileges.sql`.

Macro
-----

| Colonne       | Attribut     | Type         | Descriptions                             |
|---------------|--------------|--------------|------------------------------------------|
| id            | $id          | serial PK    |                                          |
| code          | $code        | varchar(256) | identifiant de la macro                  |
| description   | $description | text         |                                          |
| variable_name | $variable    | varchar(256) | nom de la variable utilisée par la macro |
| methode_name  | $methode     | varchar(256) | nom de la méthode utilisée par la macro  |

_Remarque :_ 
+ `variable_name` est recherchée dans le tableau de variable (si non trouvée alors un warning est mis dans le rendu `variable non trouvée`).
+ `methode_name` est appelé sur la variable `variable_name` comme suit `$variable_name->methode_name()` (si non trouvée alors un warning est mis dans le rendu `méthode non trouvée`).

Pour les ajouts des macros dans des inputs `Macro-compatible`:

- Dans le composer.json récuper les fichiers CSS/JS
```"scripts": {
        "post-install-cmd": [
            "mkdir -p public/unicaen && cp -r vendor/unicaen/renderer/public/unicaen public/",
        ]
    },
```

- Dans la page du formulaire :
```
<form>
<input type="text" name="XXX" id="XXX" class="macro-compatible" >
...
</form>
...

<?php echo $this->macroInsert() ?>

<script>
    const macros = <?php echo $macrosJsonValue ?>;
    $(function () {
        installMacrosWidgets(macros);
    });
</script>
```
`$macrosJsonValue` doit être un tableaux JSON
(Cf `TemplateController:generateMacrosJsonValueForTemplate()`) contenant les champs suivant :
+ id : Code de la macro
+ texte : VariableName de la macro
+ value : Valeur du TemplateEngine (ie : `VAR[Class#attr]`)


Template
----

| Colonne        | Attribut     | Type         | Descriptions                            |
|----------------|--------------|--------------|-----------------------------------------|
| id             | $id          | serial PK    |                                         |
| code           | $code        | varchar(256) | identifiant de la macro                 |
| engine         | $enine       | varchar(64)  | moteur ('default', 'laminas' ou 'twig') |
| description    | $description | text         |                                         |
| namespace      | $namespace   | varchar(256) | Espace de "nom" du template             |
| document_type  | $type        | text         | un type parmi 'text', 'mail', 'pdf'     |
| document_sujet | $sujet       | text         | Le sujet ou le nom du fichier           |
| document_corps | $corps       | text         | Le corps ou le contenu du fichier       |
| document_css   | $css         | text         | Un snipet CSS pour la mise en forme     |

Rendu
----

| Colonne         | Attribut  | Type       | Descriptions                    |
|-----------------|-----------|------------|---------------------------------|
| id              | $id       | serial PK  |                                 |
| template_id     | $template | integer FK | identifiant du template utilisé |
| date_generation | $date     | timestamp  |                                 |
| sujet           | $sujet    | text       | Sujet "généré"                  |
| corps           | $corps    | text       | Corps "généré"                  |

Modification du schéma
----------------------

**5.0.5**
```sql
alter table unicaen_renderer_template add namespace varchar(1024);
```

**7.0.0**
```sql
-- nouvelle colonne pour le moteur de template
create type template_engine_enum as enum('default', 'laminas', 'twig');
alter table unicaen_renderer_template
   add engine template_engine_enum default 'default' not null;

-- macros utilitaires (pas d'injection de variable nécessaire)
insert into unicaen_renderer_macro (code, description, variable_name, methode_name)
values ('Page#SautDePage', 'Saut de page', '__page', 'getSautDePage');
insert into unicaen_renderer_macro (code, description, variable_name, methode_name)
values ('Log#Date', 'Date du jour', '__log', 'getDate');
insert into unicaen_renderer_macro (code, description, variable_name, methode_name)
values ('Log#DateEtHeure', 'Date et heure du jour', '__log', 'getDateEtHeure');
```

Dépendance à **UnicaenPrivilege**
----------------------------------

1. Dans **vendor/unicaen/renderer/config/merged/index.config.php**, **vendor/unicaen/renderer/config/merged/macro.config.php**, **vendor/unicaen/renderer/config/merged/rendu.config.php**, **vendor/unicaen/renderer/config/merged/template.config.php** : `UnicaenPrivilege\Guard\PrivilegeController` pour les gardes liées aux actions.
   Peut être directement remplacer par l'equivalent fournit par `unicaen/auth`.
   
2. Dans **vendor/unicaen/renderer/src/UnicaenRenderer/Provider/Privilege/DocumentcontenuPrivileges.php**, **vendor/unicaen/renderer/src/UnicaenRenderer/Provider/Privilege/DocumentmacroPrivileges.php**, **vendor/unicaen/renderer/src/UnicaenRenderer/Provider/Privilege/DocumenttemplatePrivileges.php** : `UnicaenPrivilege\Provider\Privilege\Privileges` classe mère des privilèges du module.
   Peut être directement remplacer par l'equivalent fournit par `unicaen/auth`.

3. Utiliser UnicaenRenderer dans le cadre de UnicaenAuth, il faut écraser la config gérant les gardes de bjyauthoryze en ajoutant une config dans Application (par example `unicaen-renderer.config.php`)
```php
<?php

use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenRenderer\Controller\IndexController;
use UnicaenRenderer\Controller\MacroController;
use UnicaenRenderer\Controller\RenduController;
use UnicaenRenderer\Controller\TemplateController;
use UnicaenRenderer\Provider\Privilege\DocumentcontenuPrivileges;
use UnicaenRenderer\Provider\Privilege\DocumentmacroPrivileges;
use UnicaenRenderer\Provider\Privilege\DocumenttemplatePrivileges;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        DocumentcontenuPrivileges::DOCUMENTCONTENU_INDEX,
                        DocumentmacroPrivileges::DOCUMENTMACRO_INDEX,
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_INDEX
                    ],
                ],
                [
                    'controller' => MacroController::class,
                    'action' => [
                        'index',
                        'generer-json'
                    ],
                    'privileges' => [
                        DocumentmacroPrivileges::DOCUMENTMACRO_INDEX,
                    ],
                ],
                [
                    'controller' => MacroController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        DocumentmacroPrivileges::DOCUMENTMACRO_AJOUTER,
                    ],
                ],
                [
                    'controller' => MacroController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        DocumentmacroPrivileges::DOCUMENTMACRO_MODIFIER,
                    ],
                ],
                [
                    'controller' => MacroController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        DocumentmacroPrivileges::DOCUMENTMACRO_SUPPRIMER,
                    ],
                ],
                [
                    'controller' => RenduController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        DocumentcontenuPrivileges::DOCUMENTCONTENU_INDEX,
                    ],
                ],
                [
                    'controller' => RenduController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        DocumentcontenuPrivileges::DOCUMENTCONTENU_AFFICHER,
                    ],
                ],
                [
                    'controller' => RenduController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        DocumentcontenuPrivileges::DOCUMENTCONTENU_SUPPRIMER,
                    ],
                ],
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_INDEX,
                    ],
                ],
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_AFFICHER,
                    ],
                ],
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_AJOUTER,
                    ],
                ],
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_MODIFIER,
                    ],
                ],
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'detruire',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],
];
```
   
