INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('documentmacro', 'Gestion des macros ', 1000, 'UnicaenRenderer\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'documentmacro_index', 'Accéder à l''index', 0 UNION
    SELECT 'documentmacro_ajouter', 'Ajouter', 10 UNION
    SELECT 'documentmacro_modifier', 'Modifier', 20 UNION
    SELECT 'documentmacro_supprimer', 'Supprimer', 50
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'documentmacro';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('documenttemplate', 'Gestion des templates ', 1100, 'UnicaenRenderer\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'documenttemplate_index', 'Accéder à l''index', 0 UNION
    SELECT 'documenttemplate_ajouter', 'Ajouter', 10 UNION
    SELECT 'documenttemplate_modifier', 'Modifier', 20 UNION
    SELECT 'documenttemplate_supprimer', 'Supprimer', 50
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'documenttemplate';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('documentcontenu', 'Gestion des rendus ', 1200, 'UnicaenRenderer\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'documentcontenu_index', 'Accéder à l''index', 0 UNION
    SELECT 'documentcontenu_ajouter', 'Ajouter', 10 UNION
    SELECT 'documentcontenu_modifier', 'Modifier', 20 UNION
    SELECT 'documentcontenu_supprimer', 'Supprimer', 50
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'documentcontenu';
