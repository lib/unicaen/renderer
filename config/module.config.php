<?php

use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;

return [
    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenRenderer\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/UnicaenRenderer/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache' => [
            'apc' => [
                'namespace' => 'UNICAEN-RENDERER__' . __NAMESPACE__,
            ],
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'view_helpers' => [
        'aliases' => [
        ],
        'factories' => [
        ],
    ],

    'public_files' => [
        'inline_scripts' => [
            '10000_renderer' => '/unicaen/renderer/js/unicaen-renderer.js',
        ],
        'stylesheets' => [
            '10000_renderer' => '/unicaen/renderer/css/unicaen-renderer.css',
        ],
    ],
];