<?php

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenRenderer\Controller\MacroController;
use UnicaenRenderer\Controller\MacroControllerFactory;
use UnicaenRenderer\Form\Macro\MacroForm;
use UnicaenRenderer\Form\Macro\MacroFormFactory;
use UnicaenRenderer\Form\Macro\MacroHydrator;
use UnicaenRenderer\Form\Macro\MacroHydratorFactory;
use UnicaenRenderer\Provider\Privilege\DocumentmacroPrivileges;
use UnicaenRenderer\Service\Macro\MacroService;
use UnicaenRenderer\Service\Macro\MacroServiceFactory;
use UnicaenRenderer\View\Helper\MacroInsertViewHelper;
use UnicaenRenderer\View\Helper\RenduViewHelper;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => MacroController::class,
                    'action' => [
                        'index',
                        'generer-json'
                    ],
                    'privileges' => [
                        DocumentmacroPrivileges::DOCUMENTMACRO_INDEX,
                    ],
                ],
                [
                    'controller' => MacroController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        DocumentmacroPrivileges::DOCUMENTMACRO_AJOUTER,
                    ],
                ],
                [
                    'controller' => MacroController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        DocumentmacroPrivileges::DOCUMENTMACRO_MODIFIER,
                    ],
                ],
                [
                    'controller' => MacroController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        DocumentmacroPrivileges::DOCUMENTMACRO_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'contenu' => [
                'child_routes' => [
                    'macro' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/macro',
                            'defaults' => [
                                /** @see MacroController::indexAction() */
                                'controller' => MacroController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'generer-json' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/generer-json/:engine',
                                    'defaults' => [
                                        /** @see MacroController::genererJsonAction() */
                                        'action' => 'generer-json',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see MacroController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:macro',
                                    'defaults' => [
                                        /** @see MacroController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:macro',
                                    'defaults' => [
                                        /** @see MacroController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            MacroService::class => MacroServiceFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            MacroForm::class => MacroFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            MacroHydrator::class => MacroHydratorFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            MacroController::class => MacroControllerFactory::class,
        ]
    ],

    'view_helpers' => [
        'invokables' => [
            'macroInsert' => MacroInsertViewHelper::class,
        ],
    ],
];
