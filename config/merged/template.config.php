<?php

namespace UnicaenRenderer;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenRenderer\Controller\TemplateController;
use UnicaenRenderer\Controller\TemplateControllerFactory;
use UnicaenRenderer\Form\Template\TemplateForm;
use UnicaenRenderer\Form\Template\TemplateFormFactory;
use UnicaenRenderer\Form\Template\TemplateHydrator;
use UnicaenRenderer\Form\Template\TemplateHydratorFactory;
use UnicaenRenderer\Provider\Privilege\DocumenttemplatePrivileges;
use UnicaenRenderer\Service\Template\TemplateService;
use UnicaenRenderer\Service\Template\TemplateServiceFactory;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManager;
use UnicaenRenderer\Service\TemplateEngineManager\TemplateEngineManagerFactory;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_INDEX,
                    ],
                ],
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_AFFICHER,
                    ],
                ],
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_AJOUTER,
                    ],
                ],
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_MODIFIER,
                    ],
                ],
                [
                    'controller' => TemplateController::class,
                    'action' => [
                        'detruire',
                    ],
                    'privileges' => [
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'contenu' => [
                'child_routes' => [
                    'template' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/template',
                            'defaults' => [
                                'controller' => TemplateController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:template',
                                    'defaults' => [
                                        'controller' => TemplateController::class,
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter[/:engine]',
                                    'defaults' => [
                                        'controller' => TemplateController::class,
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:template',
                                    'defaults' => [
                                        'controller' => TemplateController::class,
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'detruire' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/detruire/:template',
                                    'defaults' => [
                                        'controller' => TemplateController::class,
                                        'action' => 'detruire',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            TemplateService::class => TemplateServiceFactory::class,
            TemplateEngineManager::class => TemplateEngineManagerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            TemplateForm::class => TemplateFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            TemplateHydrator::class => TemplateHydratorFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            TemplateController::class => TemplateControllerFactory::class,
        ]
    ],
];
