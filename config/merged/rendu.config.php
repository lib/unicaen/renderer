<?php

namespace UnicaenRenderer;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenRenderer\Controller\RenduController;
use UnicaenRenderer\Controller\RenduControllerFactory;
use UnicaenRenderer\Provider\Privilege\DocumentcontenuPrivileges;
use UnicaenRenderer\Service\Rendu\RenduService;
use UnicaenRenderer\Service\Rendu\RenduServiceFactory;
use UnicaenRenderer\View\Helper\RenduViewHelper;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => RenduController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        DocumentcontenuPrivileges::DOCUMENTCONTENU_INDEX,
                    ],
                ],
                [
                    'controller' => RenduController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        DocumentcontenuPrivileges::DOCUMENTCONTENU_AFFICHER,
                    ],
                ],
                [
                    'controller' => RenduController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        DocumentcontenuPrivileges::DOCUMENTCONTENU_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'contenu' => [
                'child_routes' => [
                    'rendu' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/rendu',
                            'defaults' => [
                                'controller' => RenduController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:rendu',
                                    'defaults' => [
                                        'controller' => RenduController::class,
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:rendu',
                                    'defaults' => [
                                        'controller' => RenduController::class,
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            RenduService::class => RenduServiceFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ],
    'controllers' => [
        'factories' => [
            RenduController::class => RenduControllerFactory::class,
        ]
    ],
    'view_helpers' => [
        'invokables' => [
            'rendu' => RenduViewHelper::class,
        ],
    ],
];
