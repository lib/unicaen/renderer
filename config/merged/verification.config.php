<?php

use Laminas\Router\Http\Literal;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenRenderer\Controller\VerificationController;
use UnicaenRenderer\Controller\VerificationControllerFactory;
use UnicaenRenderer\Provider\Privilege\DocumentcontenuPrivileges;
use UnicaenRenderer\Provider\Privilege\DocumentmacroPrivileges;
use UnicaenRenderer\Provider\Privilege\DocumenttemplatePrivileges;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => VerificationController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        DocumentcontenuPrivileges::DOCUMENTCONTENU_INDEX,
                        DocumentmacroPrivileges::DOCUMENTMACRO_INDEX,
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_INDEX
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-renderer' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/unicaen-renderer',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'verification' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/verification',
                            'defaults' => [
                                /** @see VerificationController::indexAction() */
                                'controller' => VerificationController::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
        ],
    ],
    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'factories' => [
        ],
    ],
    'controllers' => [
        'factories' => [
            VerificationController::class => VerificationControllerFactory::class,
        ]
    ],
];
