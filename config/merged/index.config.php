<?php

use Laminas\Router\Http\Literal;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenRenderer\Controller\IndexController;
use UnicaenRenderer\Controller\IndexControllerFactory;
use UnicaenRenderer\Provider\Privilege\DocumentcontenuPrivileges;
use UnicaenRenderer\Provider\Privilege\DocumentmacroPrivileges;
use UnicaenRenderer\Provider\Privilege\DocumenttemplatePrivileges;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        DocumentcontenuPrivileges::DOCUMENTCONTENU_INDEX,
                        DocumentmacroPrivileges::DOCUMENTMACRO_INDEX,
                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_INDEX
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'contenu' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/contenu',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
        ],
    ],
    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'factories' => [
        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ]
    ],
];
