CHANGELOG
=========

7.0.5
-----
- Fix composer.json : mention de la config "psr-4" pour l'autoloading.

7.0.4
-----
- Remise en place de la possibilité de générer le JSON associé aux macros (avec tous les moteurs)
- la fonction pour générer le pdf passe la main à la fonction dédiée dans unicaen/pdf

7.0.3
-----
- [update] Code JS, CSS et ViewHelper pour l'ajout des macros
- Test avec TinyMCE 7.6.0 valide

7.0.2
-----
- [FIX] Correction des dysfonctionnements en lien avec TinyMCE (tests probants avec la v6.3.0). 

7.0.1
-----
- Fix : Initialisation du service TemplateEngineManager pour VerificationController

7.0.0
-----

- Possibilité d'utiliser 2 autres moteurs de template : Laminas (PhpRenderer) et Twig.
- Apparition de macros utilitaires (Page#SautDePage, Log#Date, Log#DateEtHeure) alimentées par des variables injectées systématiquement.
- Création du CHANGELOG.md en bonne et due forme.

6.0.3 (19/09/2023)
------------------

- Ajout du filtre des types de template sur l'index des template
- Ajout du filtre sur l'index des rendus (date + template)
- Amélioration de la documentation

5.0.5
-----

- Ajout de la notion de namespace au template pour facilitité l'exploitation
